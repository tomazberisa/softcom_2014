\documentclass[journal,10pt,a4paper]{IEEEtran}
% DRAFT
%\documentclass[draftcls,12pt,a4paper, onecolumn]{IEEEtran}
 % SUBMIT, IEEE transcom
%\documentclass[draftclsnofoot, 12pt,letterpaper, onecolumn]{IEEEtran}
\usepackage [english]{babel}
\usepackage {graphicx}
\usepackage [utf8]{inputenc} %za hrv slova
% \usepackage[cp1250]{inputenc} -- Zeljko, nemoj koristiti cp1250, to je depricated i radi kako treba jedino na Winowsima. UTF-8 je preporuceno rjesenje.
\usepackage {url}
\usepackage{latexsym}
\usepackage {amsmath}
\usepackage {mathtools}
\usepackage {multirow}
\usepackage {nth}
\newcommand{\engl}[1]{engl. \textit{#1}}
\newcommand{\napomena}[1]{\textbf{!!! #1 !!!}}
\graphicspath{{png/}{eps/}}
\ifCLASSINFOpdf \else \fi

%\hyphenation{op-tical net-works semi-conduc-tor}

\begin{document}
\title{Capacity analysis of RT-based VDSL2 copper access networks}
\author{{Vedran Mikac, Željko Ilić, Tomaž Beriša, Alen Bažant, Mladen Kos, Goran Jurin, and Velimir Švedek}% <-this % stops a space
\thanks{V. Mikac is with the Ericsson Nikola Tesla d.d., HR-10000 Zagreb, Croatia, e-mail: vedran.mikac@ericsson.com.}% <-this % stops a space
\thanks{Ž. Ilić, T.Beriša, A. Bažant and M. Kos are with the Dept. of Telecommunications, Faculty of electrical engineering and computing, University of Zagreb,
HR-10000 Zagreb, Croatia, e-mail: \{zeljko.ilic, tomaz.berisa, alen.bazant, mladen.kos\}@fer.hr.}% <-this % stops a space
\thanks{G. Jurin and V. Švedek are with the Croatian Post and Electronic Communications Agency,HR-10000 Zagreb, Croatia, e-mail: \{goran.jurin, velimir.svedek\}@hakom.hr.}% <-this % stops a space

\thanks{This work was supported by Croatian Post and Electronic Communications Agency.}}


% \IEEEpeerreviewmaketitle
\maketitle
\begin{abstract}
Attenuation and crosstalk are the dominant problems in high-speed communications over copper-based digital subscriber lines (DSL). 
In order to minimize attenuation and maximize the data rate (or capacity) of twisted pair lines, xDSL operators try to keep twisted pair lines as short as possible. 
This is often achieved by deploying xDSL technologies (i.e., VDSL2) close to the end users using remote terminals (RT). 
However, since xDSL lines deployed from an RT can share a cable binder with lines deployed from the central office (CO), the resulting crosstalk may cause severe performance degradation for lines deployed from the CO. 
This is often termed as the \emph{near-far problem}.
In order to reduce crosstalk resulting from the near-far configuration, the transmit power on RT lines may be allocated to optimize for minimal impact on the CO lines.
Power Backoff (PBO) spectrum management techiniques at the RT may be used in these situations
Although this may alleviate the near-far problem, reducing transmit power of RT based xDSL systems will not remove crosstalk created within the binder group. 
Therefore, implementing vectorization techniques to cancel crosstalk between lines is highly recommended, especially for short lines


In this paper we calculate potentially achievable data rates in the downstream direction for very short (RT deployed) very-high data-rate digital subscriber lines.
Our findings are supported by laboratory measurements of attenuation and far-end crosstalk (FEXT), which take into account downstream PBO (DPBO) and vectoring techniques.
The presented results provide useful bounds for developers, xDSL providers, and relevant regulatory agencies.
\end{abstract}


\begin{IEEEkeywords}
VDSL2, data rate, crosstalk, attenuation, measurements, remote terminal, DPBO, vectoring.
\end{IEEEkeywords}

\vspace{-5pt}\section{Introduction}

\IEEEPARstart{D}IGITAL subscriber line (DSL) technology refers to a family of technologies that provide digital broadband access over the local twisted pair telephone network.
User demands for high-speed broadband access have increased the deployment of VDSL2 (Very high bit-rate Digital Subscriber Line) \cite{vdsl2} technology.
In order to maximize the data rate of twisted pair lines, they should be kept as short as possible to minimize the effect of attenuation.
Therefore, a favorable deployment scenario for xDSL technologies (such as VDSL2) is via remote terminals (RT) \cite{vangorp}.
In this scenario, subscriber lines originating at the RT share the same cable binder with lines originating at the central office (CO), which leads to significant crosstalk increase and performance degradation for the CO lines. 
This is often termed as the \emph{near-far problem}.


The near-far problem can be mitigated by using dynamic spectrum management (DSM) techniques \cite{starr}, \cite{sorbara} to optimize the transmit spectra of all users in a way that the overall data rate is maximized.
However, DSM techniques require complete real-time knowledge of crosstalk and attenuation on all twisted pairs in the binder, resulting in a computationally intensive implementation, which often becomes a deployment barrier.
To overcome these problems, a spectrum management technique referred to as Power Backoff (PBO) can be used.
In addition to being less computationally intensive, PBO needs only limited knowledge of the copper access network topology and assumes that the transmit spectra for the CO lines are fixed and can not be optimized or changed \cite{kerpez}.
The aim of PBO is to reduce the (upstream or downstream) transmit power of VDSL2 systems so that they minimally disturb systems deployed at the CO.


Reducing the transmit power of VDSL2 systems will not remove crosstalk (FEXT) between systems deployed at the RT.
On order to remove the majority of crosstalk, a form of DSM termed vectoring can be implemented at the RT.
Vectoring, as defined by ITU-T G.993.5, can greatly improve the performance of VDSL2 \cite{oksman}.
Vectoring removes the majority of FEXT created within a vectored group, by coordinating line signals \cite{ginis}.


In this paper we present performance evaluations of VDSL systems with respect to pair length and VDSL2 operational band plans (short names B8-12 and B8-6, \cite{vdsl2}), with the goal of studying the upper limit of the data rates that can be achieved at each scenario.
Also, we provide results of applying DPBO and vectoring techniques in an operating VDSL2 access network.


The remainder of this paper is organized as follows.
Section II presents the system model.
Performance evaluation results are presented in Section III and conclusions are given in Section IV.

%
%SYSTEM MODEL
%


\section{System model}

The presented performance evaluation was performed under the assumption that all transmission systems in the cable binder use DMT (Discrete Multitone) modulation \cite{pierrugues}.
Therefore, the cable binder can be approximately\footnote{Interference from binder pairs is calculated using the FSAN method of summing crosstalk \cite{golden}.} modeled using the following equation: 
\begin{equation}
\mathbf{y} = \mathbf{H} \cdot \mathbf{x} + \mathbf{z}
\end{equation}

%In this article, we only analyze downstream direction, so matrix $\mathbf{y}$ contains signal that is received at the customer premises equipment (CPE) side. Matrix %$\mathbf{x}$ contains all the input signals in the cable. Therefore, $\mathbf{x}$ contains signals that are generated at the CO or RT side.

Our analysis encompasses downstream transmission, therefore matrices $\mathbf{y}$ and $\mathbf{x}$ describe signals at the receive side (i.e., customer premises equipment (CPE) side) and signals at the transmit side (CO or RT), respectively.
Matrix $\mathbf{z}$ represents Additive White Gaussian Noise (AWGN), where $AWGN = -140$ dBm.
The channel transfer matrix $\mathbf{H}$ contains direct (diagonal elements) and far-end crosstalk (off-diagonal elements) transfer functions for pairs in the cable binder.


In our experience, existing work dealing with spectrum management in near-far scenarios seldom mention how off-diagonal elements of $\mathbf{H}$ are calculated.
In this case we use the method proposed in \cite{bingham}, where crosstalk between twisted pairs of different length can be calculated using:

\begin{equation}
|H_{FEXT}(l, l_i, f)|^2 = K_{FEXT} \cdot f^2 \cdot  l_i \cdot  |H_{CH}(l,f)|^2 
\end{equation}
where $K_{FEXT}$  [$\textrm{Hz}^{-2}\textrm{m}^{-1}$] is the crosstalk transfer coefficient, $f$ [Hz] is the disturber signal frequency, $l_i$ [m] is the interaction length, $|H_{CH}(l,f)|^2$ is direct transfer function of the channel (i.e., cable), and $l$ [m] is the total distance from the CO (or RT) to the CPE of the pair that generates noise on the pair that is being disturbed.
It should be stressed that this equation assumes both pairs have the same attenuation constant.


Also, we assume that all modems in the cable are synchronized and that there is no intercarrier interference.
Due to the fact that all transmission systems in this analysis use frequency division duplexing, near-end crosstalk (NEXT) noise is not considered.


The maximum bitloading a modem can achieve under the current standards is $b_{max} = 15$ bits per tone \cite{vdsl2}.
In all calculations, we assume discrete bitloading.
For every tone $k$ and channel $n$, bitloading can be calculated using the equation:

\begin{equation}
b_k^n = \left \lfloor{ min {\left ( b_{max} , \log_2 \left (1 + \frac{S_k^n}{\Gamma \cdot N_k^n} \right ) \right ) }} \right \rfloor  
\label{eq:shannon1}
\end{equation}
where $S_k^n$ is signal power at the CPE side, $N_k^n$ is the sum of crosstalk from all the DMT based disturbers in the cable, and $\Gamma$ is the \textit{SNR}-gap \cite{bingham}.

%
% SUBSECTION 
% Measured Cables
%

\subsection{Measured cables}
\label{sec:measured_cables}
Measurements were performed on multicore symmetric quad cables that conform to \cite{kabel}.
Cable conductors are manufactured using softly annealed copper, where the insulation consists of foamed polyethylene with a solid polyethylene jacket and laminated sheath.

Cable binders have the following structure.
The smallest building block of binder cables is a \emph{quad}, which consists of two twisted pairs.
The next building block according to size is a \emph{basic group} and consists of five quads (ten pairs).
Larger cables are obtained by gathering multiple basic groups together to form a \emph{main group} (five basic groups) or \emph{super group} (five main groups). 
In this paper all calculations were performed on one basic group or one main group.
%In this paper all calculations were performed under the assumption that cable consists, depending on the scenario, of one main group or one basic group. 


The following measurements were performed on available cables: 

\begin{itemize}
\item pair attenuation,
\item far-end crosstalk.
\end{itemize}

Measured cables were between 120 m and 170 m in length, and consisted of one main group or one super group.
From the cable construction it is possible to pick out the following relationships between pairs in a cable:

\begin{itemize}
\item [A1:] pairs are in the same quad;
\item [A2:] pairs are in neighboring quads in the same basic group;
\item [A3:] pairs are in the same basic group, but separated by a quad;
\item [B1:] pairs are in neighboring basic groups in one main group;
\item [B2:] pairs are in the same main group, but separated by a basic group.
\end{itemize}


Measurements were carried out using the AESA 9600/100 MHz (now renamed to PHOENIX) measuring system from AESA Cortalliod \cite{aesa}.
It can measure up to 112 pairs at once in frequency ranges up to 100 MHz.
It is important to stress that the measurements were performed in laboratory environment and the measured cables were on a reel, thus no outside noise and other effects such as humidity in a cable affected the measurement results.
Although cables should be stretched out so the self-crosstalk from cable winding on the reel is avoided, performing measurements on the reel was not an issue in our case because the cables were shielded and the shield was properly grounded.


Finally, all the measurement results were normalized to a length of 100 m and were conducted in the frequency range between 64 kHz and 100 MHz.

\subsection{Insertion loss model}

The insertion loss model used in this paper is consistent with \cite{ITU-T-G.997.1}:

\begin{equation}
A(f, l) = (k_1 + k_2 \sqrt{f} + k_3 f) \cdot l
\end{equation}
where $k_1$, $k_2$, and $k_3$ are constants and $l$ [km] is the pair length.

Insertion loss analysis performed on available cables produced the following constants: $k_1 = 6.033$ [$\textrm{dB/Hz}^{0.5} \textrm{km}^{-1}$], $k_2 = 1.682 \cdot 10^{-2}$ [$\textrm{dB/Hz}^{0.5}\textrm{km}^{-1}$] and $k_3 = 1.410 \cdot 10^{-6}$ [db/Hz$\textrm{km}^{-1}$].


\subsection{Far-end Crosstalk models}

All pairs in the measured cables were the same length, so the standard ETSI FEXT power transfer function was used to model measurements on the real cables \cite{golden}:

\begin{equation}
|H_{FEXT}(l, f)|^2 = K_{FEXT} \cdot f^2 \cdot l \cdot |H_{CH}(f,l)|^2 
\end{equation}
where $|H_{FEXT}(l, f, N)|^2$ is crosstalk power transfer function to the far-end of the pair, $f$ is signal frequency in [Hz], $l$ is interaction length between two pairs, $|H_{CH}(f,l)|^2$ is the direct pair transfer function, and $K_{FEXT}$ [$\textrm{Hz}^{-2}\textrm{m}^{-1}$] is the crosstalk coefficient.


Cable analysis produced results presented in Table \ref{tab:fext_constants}.
For all victim-aggressor pair combinations specified in \ref{sec:measured_cables}, we present both an average model obtained by using least squared fitting method and 1\%-worst case model calculated under the assumption that crosstalk is distributed according to the normal distribution.


\begin{table}[!ht]
\caption{Far end crosstalk constants derived from available measurements}
\centering
\begin{tabular}{|c|c|c|}
\hline
Pair combination & $K_{FEXT}$ & $K_{FEXT,1\%}$ \\ \hline
A1  & $10^{-20.2345}$ & $10^{-18.4434}$ \\ \hline
A2  & $10^{-20.4280}$ & $10^{-18.8854}$ \\ \hline
A3  & $10^{-20.4275}$ & $10^{-18.7955}$ \\ \hline
B1  & $10^{-21.1753}$ & $10^{-19.2839}$ \\ \hline
B2  & $10^{-21.9331}$ & $10^{-20.3500}$ \\ \hline
\end{tabular}
\label{tab:fext_constants}
\end{table}


%
% SECTION 
% Downstream Capacity Calculation
%

\section{Downstream Capacity Results}
\label{sec:results}

This section presents results that were obtained using the calculator \cite{CO-RT-CPEcalc} developed as a result of the collaboration between FER\footnote{Faculty of electrical engineering and computing, University of Zagreb, Croatia.} and HAKOM\footnote{Croatian Post and Electronic Communications Agency.}.
The following scenarios were investigated:
\begin{enumerate}
\item impact of downstream VDSL2 transmission system spectral mask shaping on the achievable rates of VDSL2 and ADSL2+ \cite{adsl2Plus} systems that originate at the RT and CO, respectively;
\item impact of vectorization on VDSL2 systems that originate at RT;
\item optimal VDSL2 profiles for Fiber To The Node (FTTN) applications.
\end{enumerate}

\subsection{Analysis of maximum access network data rate increase through introduction of RTs}
\label{sec:res01}
This subsection presents achievable data rates as a function of subscriber loop length for the VDSL2 998 B8-12 transmission system at the RT.
We present possible configurations where data rate is greater than 30 Mbit/s in the downstream direction.


Calculations were performed under the following assumptions:
\begin{itemize}
\item subscribers are connected using 0.4 mm cables throughout all the segments of the connection (CO - RT - CPE);
\item analysis was performed on a basic group;
\item CO-RT length is 1300 m;
\item DPBOESEL is obtained from cable properties (length and insertion loss);
\item RT-CPE length varies from 300 to 1200 m (when the data rate drops below 30 Mbit/s);
\item Transmission system for the subscribers that are directly connected to the CO is ADSL2+ AnnexB;
\item Transmission system for the subscribers that are connected to RT is VDSL2 998 B8-12;
\item Fill ratio of the subscribers that are directly connected to the CO is 10\% and 20\%;
\item Fill ratio of the subscribers that are connected to RT is 40\% and 80\%;
\item DPBO calculation was done with the following parameters \cite{study}:
\begin{itemize}
\item DPBOESCMA = 0.2487
\item DPBOESCMB = 0.6932
\item DPBOESCMC = 0.0581
\item DPBOMUS = -95 dBm
\item DPBOFMIN = 138 kHz
\item DPBOFMAX = 2208 kHz
\end{itemize}
\end{itemize}

\begin{figure}[!ht]
\centering
\vspace{-9pt}
\includegraphics[width=8cm]{res01}
\vspace{-17pt}
\caption{Date rate to subscriber loop length dependency for VDSL2 998 B8-12 transmission system originating at the RT}
\label{fig:res01}
\end{figure}

Figure~\ref{fig:res01} shows that required data rate of 30 Mbit/s or more is achieved for loops that are shorter than:
\begin{itemize}
\item 600 m for 80\% VDSL2 998 B8-12 and 20\% ADSL2+ AnnexB fill ratio;
\item 710 m for 40\% VDSL2 998 B8-12 and 10\% ADSL2+ AnnexB fill ratio.
\end{itemize}

%
% SUBSECTION 
% Impact of Vectorization at RT
%

\subsection{Impact of vectorization at RT}

This subsection presents achievable data rate as a function of subscriber loop length for \emph{vectored} VDSL2 998 B8-12 transmission system at the RT.
We present possible configurations where data rate is greater than 30 Mbit/s in downstream direction.

Scenario parameters are the same as in subsection \ref{sec:res01} with the following additions:
\begin{itemize}
\item analysis is performed on the main group (instead of basic group) that consists of 25 quads (or 50 pairs);
\item VDSL2 998 B8-12 systems have 40\% fill ratio, and ADSL2+ AnnexB have 10\% fill ratio.
\end{itemize}

Analysis was performed for the following vectorization scenarios:
\begin{enumerate}
\item \label{scen2a} vectoring was not employed;
\item \label{scen2b} crosstalk from the pairs outside the basic group was completely canceled; 
\item \label{scen2c} crosstalk from pairs inside the basic group was completely canceled.
\end{enumerate}


\begin{table}[!ht]
\caption{Crosstalk from the pairs outside the basic group is canceled}
\centering
\begin{tabular}{|c|c|c|c|}
\hline
\multirow{2}{*}{Length [m]} & \multicolumn{2}{c|}{Data rate [Mbit/s]} & \multirow{2}{*}{$\Delta$\textit{R} [\%]} \\ \cline{2-3}
 & (no vect.) & (vect.) & \\ \hline
1300&140.04&143.16&2.2\\ \hline
1400&80.23&83.89&4.56\\ \hline
1500&61.56&64.5&4.77\\ \hline
1600&51.28&53.98&5.26\\ \hline
1700&44.7&46.8&5.1\\ \hline
1900&34.14&35.84&4.97\\ \hline
2000&29.02&30.29&4.37\\ \hline
2100&26.24&27.16&3.5\\ \hline
2300&19.99&20.56&2.85\\ \hline
2500&14.83&15.22&2.62 \\ \hline
\end{tabular}
\label{tab:vect1}
\end{table}

\begin{table}[!ht]
\caption{Crosstalk from pairs inside the basic group is canceled}
\centering
\begin{tabular}{|c|c|c|c|}
\hline
\multirow{2}{*}{Length [m]} & \multicolumn{2}{c|}{Data rate [Mbit/s]} & \multirow{2}{*}{$\Delta$\textit{R} [\%]} \\ \cline{2-3}
 & (no vect.) & (vect.) & \\ \hline
1300&140.04&157.5&12.46\\ \hline 
1400&80.23&99.59&24.13\\  \hline 
1500&61.56&78.92&28.2\\ \hline 
1600&51.28&67.5&31.63\\ \hline 
1700&44.7&59.18&32.39\\ \hline 
1900&34.14&44.43&30.14\\ \hline 
2000&29.02&36.25&2491\\ \hline 
2100&26.24&31.97&21.83\\ \hline 
2300&19.99&23.62&18.15\\ \hline 
2500&14.83&17.06&15.03\\ \hline
\end{tabular}
\label{tab:vect2}
\end{table}




\begin{figure}[!ht]
\centering
\vspace{-9pt}
\includegraphics[width=8cm]{res02}
\vspace{-17pt}
\caption{Data rate to loop length dependency for VDSL2 998 B8-12 for scenarios \ref{scen2a}, \ref{scen2b} and \ref{scen2c}}
\label{fig:res02}
\end{figure}


Table \ref{tab:vect1}, Table \ref{tab:vect2}, and Figure~\ref{fig:res02} present calculation results for the given scenarios.
Please note that $\Delta$\textit{R} (in tables) presents a relative difference between vectored and non-vectored data rates.
 

Results for both scenarios show that vectorization increases data rate and maximum loop length for subscribers that are connected to the RT by:
\begin{enumerate}
\item 5.1\% increase in achievable data rate and 4.41\% increase in maximum subscriber loop length (maximum subscriber loop length is increased by 30 m) in the case where crosstalk from the pairs outside the basic group was canceled;
\item up to 33\% increase in achievable data rate and 25\% increase in maximum subscriber loop length (maximum subscriber loop length is increased from 680 m to 850 m) in the case where crosstalk from pairs inside the basic group is canceled 
\end{enumerate} 

%
% SUBSECTION 
% Optimal VDSL2 profiles for the FTTN applications
%

\subsection{Optimal VDSL2 profiles for the FTTN applications}

This subsection presents achievable data rate as a function of subscriber loop length for VDSL2 998 B8-6 and VDSL2 998 B8-12 transmission systems in the downstream direction.
In this scenario the following was examined:
\begin{itemize}
\item \label{scen3a} Whether VDSL2 998 B8-6 or VDSL2 998 B8-12 is better suited for loops shorter than 700 m (average loop length), \textit{without} combining different transmission systems in the cable binder.
The analysis includes 50\% and 100\% fill ratios for the basic group.
\item \label{scen3b} Impact of combining transmission systems in the cable on the data rates for VDSL2 998 B8-6 and VDSL2 998 B8-12 with 20\% and 30\% fill ratios, respectively;
\end{itemize}

Results for scenario \ref{scen3a} are presented in Table \ref{tab:scen3a1}, Table \ref{tab:scen3a2}, and Figure \ref{fig:res03}.
Results show that VDSL2 998 B8-12 has greater data rates than VDSL2 998 B8-6 for loops up to 550 m in length with 50\% and 100\% fill ratios.

\begin{table}[!ht]
\caption{Data rate to loop length dependency for VDSL2 998 B8-6 and VDSL2 998 B8-12 with 50\% fill ratio}
\centering
\begin{tabular}{|c|c|c|}
\hline
\multirow{3}{*}{Length [m]} & \multicolumn{2}{c|}{50\% fill ratio} \\ \cline{2-3}
 & Data rate [Mbit/s] &  Data rate [Mbit/s]\\ 
 & 5 x VDSL2 998 B8-6 & 5 x VDSL2 998 B8-12 \\ \hline
100&46.2&81.42\\ \hline
200&39.97&62.7\\ \hline
300&36.16&52.44\\ \hline
400&33.79&46.24\\ \hline
500&32.13&41.38\\ \hline
600&30.15&36.34\\ \hline
700&28.67&31.76\\ \hline
\end{tabular}
\label{tab:scen3a1}
\end{table}

\begin{table}[h]
\caption{Data rate to loop length dependency for VDSL2 998 B8-6 and VDSL2 998 B8-12 with 100\% fill ratio}
\centering
\begin{tabular}{|c|c|c|}
\hline
\multirow{3}{*}{Length [m]} & \multicolumn{2}{c|}{100\% fill ratio} \\ \cline{2-3}
 & Data rate [Mbit/s] &  Data rate [Mbit/s]\\ 
 & 10 x VDSL2 998 B8-6 & 10 x VDSL2 998 B8-12 \\ \hline
100&40.55&69.35\\ \hline
200&34.38&50.52\\ \hline
300&31.05&42.28\\ \hline
400&28.34&36.97\\ \hline
500&26.65&32.97\\ \hline
600&25.39&29.51\\ \hline
700&23.68&25.8\\ \hline
\end{tabular}
\label{tab:scen3a2}
\end{table}

\begin{figure}[h]
\centering
\vspace{-9pt}
\includegraphics[width=8cm]{res03}
\vspace{-17pt}
\caption{Data rate to loop length dependency for VDSL2 998 B8-12 and VDSL2 998 B8-6 with fill ratios of 50\% and 100\%, without combining systems in cable binder}
\label{fig:res03}
\end{figure}

Table \ref{tab:scen3b1} and Figure \ref{fig:res04} show the data rates for VDSL2 998 B8-6 and VDSL2 998 B8-12 when combined in the same basic group.
So, it is not recommended at shorter subscriber loop lengths (up to 600 m) above two mentioned VDSL2 technologies use in the same cable group.


\begin{table}[h]
\caption{Data rate to loop length dependency for VDSL2 998 B8-12 and VDSL2 998 B8-6 with 30\% and 20\% fill ratio, respectively}
\centering
\begin{tabular}{|c|c|c|}
\hline
\multirow{3}{*}{Length [m]} & \multicolumn{2}{c|}{50\% fill ratio} \\ \cline{2-3}
 & Data rate [Mbit/s] &  Data rate [Mbit/s]\\ 
 & 3 x VDSL2 998 B8-12 & 2 VDSL2 998 B8-6 \\ \hline
100&82.18&47.8\\ \hline
200&62.41&41.56\\ \hline
300&52.24&37.73\\ \hline
400&45.42&35.38\\ \hline
500&39.95&33.68\\ \hline
600&33.98&31.72\\ \hline
700&28.2&30.26\\ \hline
\end{tabular}
\label{tab:scen3b1}
\end{table}



\begin{figure}[!ht]
\centering
\vspace{-9pt}
\includegraphics[width=8cm]{res04}
\vspace{-17pt}
\caption{Data rate to loop length dependency for VDSL2 998 B8-12 and VDSL2 998 B8-6 with fill ratios of 30\% and 20\%, respectively}
\label{fig:res04}
\end{figure}

%
% SECTION 
% Conclusion
%

\newpage 
\section{Conclusion}
At the time of writing this paper, ADSL2+ AnnexB is the most common xDSL access technology in Croatia.
The need for data rate increase in copper based access networks and migration to the FTTN architecture definitively points towards the introduction of VDSL2 technologies.
Introduction of these transmission systems in the network mandates the use of remote terminals, which in turn increases the crosstalk that is generated on legacy ADSL2+ systems that originate at the CO and are in the same binder as new VDSL2 systems.
This problem, known as the near-far problem can be alleviated or possibly even completely resolved by applying various spectrum management techniques and vectoring.


From the results presented in section \ref{sec:results} the following can be concluded.
\begin{itemize}
\item Downstream Power Backoff, when applied with appropriate parameters, can greatly reduce noise in near-far scenarios. As a consequence, this increases data rates and ranges for transmission systems in the binder.
Using DPBO with the parameters specified in this paper brings balance between legacy systems that originate at CO and new VDSL2 systems that originate at the RT.
\item Results also show that vectorization performed within the basic group in the binder significantly increases the data rate and range of the transmission systems that originate at the RT. 
\end{itemize}

\section{Acknowledgement}
The authors of this paper acknowledge Elka d.o.o. \cite{zahvala} for cable measurements.
Vlatko Poljak is credited for performing the measurements and invaluable discussions regarding the measurement process.


\bibliographystyle{IEEEtran}

\begin{thebibliography}{20}
\bibitem{vdsl2} G.993.2. "Very high speed digital subscriber line transceivers 2 (VDSL2)", ITU-T Recomm., Dec., 2011.
\bibitem{vangorp} J. Vangorp, M. Moonen, M. Guenach, and Michael Peeters, "Downstream Power Backoff in CO/RT-Deployed xDSL Networks", \emph{IEEE Trans. on Commun.}, Vol. 58, No. 2, pp. 453-456, Feb., 2010.
\bibitem{starr} T. Starr, J. M. Cioffi, and P. J. Silverman, "Understanding Digital Subscriber Lines", Prentice Hall, 1999.
\bibitem{sorbara} T. Starr, M. Sorbara, J. M. Cioffi, and P. J. Silverman, "DSL Advances", Prentice Hall, 2003.
\bibitem{kerpez} K. Kerpez, J.M. Cioffi, S. Galli, G. Ginis, M. Goldburg, M. Mohseni, and A. Chowdhery, "Compatibility of Vectored and Non-Vectored VDSL2", \emph{Information Sciences and Systems (CISS), 2012 46th Annual Conference on}, pp. 1 - 6, DOI 10.1109/CISS.2012.6310771, 21 - 23 March 2012.
\bibitem{oksman} V. Oksman, H. Schenk, A. Clausen, J. Cioffi, M. Mohseni, G. Ginis, C. Nuzman, J. Maes, M. Peeters, K. Fisher, and P. Eriksson, "The ITUT's new G.vector standard proliferates 100 Mb/s DSL", \emph{IEEE Commun. Mag.}, Vol. 48, No. 10, pp. 140 - 148, Oct. 2010.
\bibitem{ginis}	G. Ginis, and J.M. Cioffi, "Vectored Transmission for Digital Subscriber Line Systems", \emph{IEEE Journal on Selected Areas in Comm.}, Vol. 20, No. 5, pp. 1085 - 1104, June, 2002.
\bibitem{pierrugues} L. Pierrugues, O. Moreno, P. Duvaut, F.Ouyang, „DMT performance improvement based on Clustering Modulation, applications to ADSL“, \emph{Communications, 2004 IEEE International Conference on}, DOI 10.1109/ICC.2004.1313041, pp. 2807 – 2811, 20-24 June 2004.
\bibitem{bingham} J. Bingham, "ADSL, VDSL and Multicarier Modulation", John Wiley and Sons Inc, 2000.
\bibitem{kabel} T4– 23336/92 – Tehnički uvjeti za telekomunikacijske niskofrekventne kabele sa izolacijom vodića od pjenastog polietilena i slojevitim plaštom od polietilena, HPT, 1992.
\bibitem{aesa} AESA Cortaillod - Measurement equipment documentation, 2004.
\bibitem{ITU-T-G.997.1} G.997.1. "Physical layer management for digital subscriber line (DSL) transceivers", ITU-T Recomm., June, 2012.
\bibitem{golden} P. Golden, H. Dedieu, K.S. Jacobsen, "Fundamentals of DSL Technology", Auerbach Publications, 2006.
\bibitem{CO-RT-CPEcalc} \url{http://www.fer.unizg.hr/images/752/CO-RT-CPE_Calc_BasicInfo.pdf.}
\bibitem{adsl2Plus} G.992.5. "Asymmetric digital subscriber line transceivers 2 (ADSL2) – Extended bandwidth (ADSL2plus)", 
\bibitem{study} Ž. Ilić, A. Bažant, M. Kos, T. Beriša, V. Mikac, Study "Analysis of availability and data rates of existing broadband copper access network", collaboration between Faculty of electrical engineering and computing (FER) and Croatian post and electronic communications agency (HAKOM), Zagreb, May, 2014.
\bibitem{zahvala} "Elka kabeli d.o.o.", \url{www.elka.hr.}
\end{thebibliography}
\end{document}
